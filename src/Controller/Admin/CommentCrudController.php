<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\Conference;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class CommentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Comment::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('author'),
            TextField::new('email'),
            TextField::new('text'),
            DateField::new('createdAt'),
            TextField::new('state'),
            TextField::new('photo_filename'),
            ImageField::new('photoFilename', 'Image')
                ->onlyOnIndex()
                ->setBasePath('/uploads/photos'),
            AssociationField::new('conference')
        ];
    }
}
